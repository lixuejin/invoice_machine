package com.lxj.invoiceMachine.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.kongzue.tabbar.Tab;
import com.kongzue.tabbar.interfaces.OnTabChangeListener;
import com.lxj.invoiceMachine.Adapter.MainFragmentAdapter;
import com.lxj.invoiceMachine.Base.BaseActivity;
import com.lxj.invoiceMachine.Bean.UpDataBean;
import com.lxj.invoiceMachine.Fragment.InvoiceFragment;
import com.lxj.invoiceMachine.Fragment.RecordFragment;
import com.lxj.invoiceMachine.Fragment.SetFragment;
import com.lxj.invoiceMachine.Http.Model.HttpData;
import com.lxj.invoiceMachine.Http.api.UpdataApi;
import com.lxj.invoiceMachine.PrinterUtil.SunmiPrintHelper;
import com.lxj.invoiceMachine.R;
import com.lxj.invoiceMachine.Utils.GsonUtil;
import com.lxj.invoiceMachine.Utils.PhoneUtil;
import com.lxj.invoiceMachine.databinding.ActivityMainBinding;
import com.lxj.invoiceMachine.view.UpDataDialog;
import com.orhanobut.logger.Logger;
import com.sunmi.peripheral.printer.InnerPrinterCallback;
import com.sunmi.peripheral.printer.InnerPrinterException;
import com.sunmi.peripheral.printer.InnerPrinterManager;
import com.sunmi.peripheral.printer.SunmiPrinterService;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity<ActivityMainBinding> {
    private MainFragmentAdapter mainFragmentAdapter;
    private List<Fragment> fragments = new ArrayList<>();

    @Override
    protected int getContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        //设置底部导航
        List<Tab> tabs = new ArrayList<>();
        tabs.add(new Tab(this, "加油开票", R.drawable.icon_invoice_off).setFocusIcon(this, R.drawable.icon_invoice_on));
        tabs.add(new Tab(this, "开票记录", R.drawable.icon_record_off).setFocusIcon(this, R.drawable.icon_record_on));
        tabs.add(new Tab(this, "设置", R.drawable.icon_set_off).setFocusIcon(this, R.drawable.icon_set_on));
        binding.tabbar.setTab(tabs)
                .setOnTabChangeListener(new OnTabChangeListener() {
                    @Override
                    public boolean onTabChanged(View v, int index) {
                        Log.i(">>>", "onTabChanged: " + index);
                        binding.vpMain.setCurrentItem(index);
                        return false;
                    }
                })
                .setNormalFocusIndex(0);
        //设置fragment
        fragments.add(new InvoiceFragment());
        fragments.add(new RecordFragment());
        fragments.add(new SetFragment());
        mainFragmentAdapter = new MainFragmentAdapter(getSupportFragmentManager(), getLifecycle(), fragments);
        binding.vpMain.setAdapter(mainFragmentAdapter);
        //设置滑动监听
        binding.vpMain.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                binding.tabbar.setNormalFocusIndex(position);
            }
        });
        upData();
    }

    @Override
    protected void initData() {
    }

    @Override
    public void onClick(View v) {

    }

    private void upData() {
        EasyHttp.post(this)
                .api(new UpdataApi())
                .request(new OnHttpListener<HttpData<UpDataBean>>() {
                    @Override
                    public void onSucceed(HttpData<UpDataBean> result) {
                        Logger.d(GsonUtil.GsonString(result));
                        try {
                            //获取本机
                            int code = PhoneUtil.getVersionCode(context);
                            //本机小于服务器的话
                            if (code < result.getResult().getApkVersion()) {
                                //更新
                                if (result.getResult().getApkMust() == 0) {
                                    //非强制
                                    UpDataDialog.NeedUp(getSupportFragmentManager(), context, result.getResult().getApkMessage()
                                            , result.getResult().getApkFile(), MainActivity.this);
                                }
                                if (result.getResult().getApkMust() == 1) {
                                    //强制
                                    UpDataDialog.mustUp(getSupportFragmentManager(), context, result.getResult().getApkMessage()
                                            , result.getResult().getApkFile(), MainActivity.this);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFail(Exception e) {

                    }
                });
    }
}