package com.lxj.invoiceMachine.Activity;

import androidx.appcompat.app.AppCompatActivity;
import es.dmoral.toasty.Toasty;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.hjq.http.EasyConfig;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.kongzue.tabbar.Tab;
import com.lxj.invoiceMachine.Base.AppConfig;
import com.lxj.invoiceMachine.Base.BaseActivity;
import com.lxj.invoiceMachine.Base.MyApplication;
import com.lxj.invoiceMachine.Bean.LoginBean;
import com.lxj.invoiceMachine.Http.HttpUtil;
import com.lxj.invoiceMachine.Http.Model.HttpData;
import com.lxj.invoiceMachine.Http.api.LoginForInvoicerApi;
import com.lxj.invoiceMachine.R;
import com.lxj.invoiceMachine.Utils.AESUtils;
import com.lxj.invoiceMachine.Utils.PhoneUtil;
import com.lxj.invoiceMachine.databinding.ActivityLoginBinding;
import com.zyao89.view.zloading.ZLoadingDialog;
import com.zyao89.view.zloading.Z_TYPE;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends BaseActivity<ActivityLoginBinding> {
    private ZLoadingDialog zLoadingDialog;
    @Override
    protected int getContentViewId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {
        binding.setOnClick(this);
    }

    @Override
    protected void initData() {
        String uerName = (String) MyApplication.sharedPreferences.getSharedPreference(AppConfig.UserName, "");
        String passWord = (String) MyApplication.sharedPreferences.getSharedPreference(AppConfig.PassWord, "");
        binding.etUsername.setText(uerName);
        binding.etPassword.setText(passWord);
        zLoadingDialog = new ZLoadingDialog(context);
        if(!uerName.equals("") || !passWord.equals("")){
            Login();
        }
    }

    @Override
    public void onClick(View v) {
            switch (v.getId()){
                case R.id.btn_login:
//                    goActivity(MainActivity.class);
                    Login();
                    break;
            }
    }

    private void Login() {
        String uerName = binding.etUsername.getText().toString();
        if (uerName.equals("")) {
            Toasty.warning(context, "未输入账号", Toast.LENGTH_LONG, true).show();
            return;
        }
        String passWord = binding.etPassword.getText().toString();
        if (passWord.equals("")) {
            Toasty.warning(context, "未输入密码", Toast.LENGTH_LONG, true).show();
            return;
        }
        String passWordBytes = AESUtils.encrypt(AppConfig.AESKEY.getBytes(), passWord.getBytes());
        zLoadingDialog.setLoadingBuilder(Z_TYPE.DOUBLE_CIRCLE)//设置类型
                .setLoadingColor(R.color.btn_ghost_blue_border_normal)//颜色
                .setHintTextSize(14)
                .setHintText(getString(R.string.login))
                .setCanceledOnTouchOutside(false)
                .setCancelable(false)
                .show();
        zLoadingDialog.show();
        LoginHttp(uerName, passWord, passWordBytes);
    }

    private void LoginHttp(String uerName, String passWord, String passWordBytes) {
        EasyHttp.post(this)
                .json(HttpUtil.loginForInvoicer(uerName, passWordBytes, PhoneUtil.getMac()))
                .api(new LoginForInvoicerApi())
                .request(new OnHttpListener<HttpData<LoginBean>>() {
                    @Override
                    public void onSucceed(HttpData<LoginBean> result) {
                        zLoadingDialog.dismiss();
                        // 更新 Token
                        EasyConfig.getInstance().addHeader("X-Access-Token", result.getResult().getToken());
                        //保存信息
                        MyApplication.loginBean = result.getResult();
                        //账号密码保存本地
                        MyApplication.sharedPreferences.put(AppConfig.UserName, uerName);
                        MyApplication.sharedPreferences.put(AppConfig.PassWord, passWord);
                        //跳转到主页
                        goActivity(MainActivity.class);
                    }

                    @Override
                    public void onFail(Exception e) {
                        zLoadingDialog.dismiss();
                        Toasty.warning(context, "登录失败:"+e.getMessage(), Toast.LENGTH_LONG, true).show();

                    }
                });
    }
}