package com.lxj.invoiceMachine.Utils;

import java.util.Date;
import java.util.Random;

public class InvoiceDateUtils {
    //获取当前时间

    public static String nowTime() {

        String time = DateUtil.parseDateToStr(new Date(), DateUtil.DATE_TIME_FORMAT_YYYY_MM_DD_HH_MI_SS);
        return time;
    }

    //获取20分钟前的时间
    public static String beforeTime() {
        Date now = new Date();
        Date now_20 = new Date(now.getTime() - 1200000); //10分钟前的时间
        String time = DateUtil.parseDateToStr(now_20, DateUtil.DATE_TIME_FORMAT_YYYY_MM_DD_HH_MI_SS);
        return time;
    }

    public static String orderNo() {
        String orderNo = "";
        for (int i = 0; i < 6; i++) {
            int number = new Random().nextInt(10);
            orderNo = orderNo + number;
        }
        orderNo = orderNo + DateUtil.parseDateToStr(new Date(), DateUtil.DATE_TIME_FORMAT_YYYYMMDDHHMISSSSS);
        return orderNo;
    }
}
