package com.lxj.invoiceMachine.Utils;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogUtil {

    private static final int LEV_D = 1;
    private static final int LEV_W = 2;

    public static void d(String tag, String msg) {
        if (LEV_D == 1) {
            Log.d(tag, msg);
        }
    }

    /**
     * 为避免产生大量垃圾日志文件，引入一个常量来决定是否需要打印日志
     *
     * @param msg 打印的日志消息
     */
    public static void writerlog(String msg) {
        if (LEV_W == 2) {
            //保存到的文件路径
            final String filePath = Environment.getExternalStorageDirectory().getAbsolutePath();
            FileWriter fw = null;
            BufferedWriter bw = null;

            try {
                //创建文件夹
                File dir = new File(filePath, "log");
                if (!dir.exists()) {
                    dir.mkdir();
                }
                //创建文件
                File file = new File(dir, "log.txt");
                if (!file.exists()) {
                    file.createNewFile();
                }
                //写入日志文件
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日   HH:mm:ss");
                Date curDate = new Date(System.currentTimeMillis());
                String str = formatter.format(curDate);
                fw = new FileWriter(file, true);
                bw = new BufferedWriter(fw);
                bw.write(str + ":" + msg + "\n");
                bw.close();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bw != null) {
                    try {
                        bw.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        }


    }
//    public static void writerlog(String msg){
//
//    }
}
