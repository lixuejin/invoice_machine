package com.lxj.invoiceMachine.Http.Model;

/**
 * author : Android 轮子哥
 * github : https://github.com/getActivity/AndroidProject
 * time   : 2019/12/07
 * desc   : 统一接口数据结构
 */
public class HttpData<T> {


    /**
     * 是否请求成功
     */
    private boolean success;
    /**
     * 提示语
     */
    private String message;
    /**
     * 返回码
     */
    private int code;
    /**
     * 数据
     */
    private T result;
    /**
     * 时间
     */
    private int timestamp;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

}