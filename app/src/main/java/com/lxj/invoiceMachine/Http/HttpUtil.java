package com.lxj.invoiceMachine.Http;

import com.lxj.invoiceMachine.Bean.CommodityOrdersBean;
import com.lxj.invoiceMachine.Utils.GsonUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpUtil {
    /**
     * 登录
     *
     * @param userName 用户名
     * @param passWord 密码
     * @param mac      mac地址
     * @return
     */
    public static String loginForInvoicer(String userName, String passWord, String mac) {
        Map<String, Object> map = new HashMap<>();
        map.put("userName", userName);
        map.put("passWord", passWord);
        map.put("mac", mac);
        return GsonUtil.GsonString(map);
    }

    /**
     * 获取开票二维码
     * @param orderNo 订单号
     * @param orderTotal 订单总价
     * @param taxTotal 合计税额
     * @param bhtaxTotal 不含税金额
     * @param qdbz 清单标志
     * @param qdxmmc 清单项目名称
     * @param invoicedate 单据时间
     * @param message 备注
     * @param clerk 开票人
     * @param payee 收款人
     * @param checker 复核人
     * @param deptid 部门门店 id
     * @param clerkid 开票员 id
     * @param cpybz 成 品 油 标 志
     * @param commodityOrders 明细数据
     * @param taxNum 税号
     * @return
     */
    public static String getInvoiceUrl(String orderNo, String orderTotal, String taxTotal, String bhtaxTotal, String qdbz,
                                       String qdxmmc, String invoicedate, String message, String clerk, String payee,
                                       String checker, String deptid, String clerkid, String cpybz, List<CommodityOrdersBean> commodityOrders, String taxNum) {
        Map<String, Object> map = new HashMap<>();
        map.put("orderNo", orderNo);
        map.put("orderTotal", orderTotal);
        map.put("taxTotal", taxTotal);
        map.put("bhtaxTotal", bhtaxTotal);
        map.put("qdbz", qdbz);
        map.put("qdxmmc", qdxmmc);
        map.put("invoicedate", invoicedate);
        map.put("message", message);
        map.put("clerk", clerk);
        map.put("payee", payee);
        map.put("checker", checker);
        map.put("deptid", deptid);
        map.put("clerkid", clerkid);
        map.put("cpybz", cpybz);
        map.put("commodityOrders", commodityOrders);
        map.put("taxNum", taxNum);
        return GsonUtil.GsonString(map);
    }
    public static String QueryOrderByOilGun(String oilGun, String pageNo) {
        Map<String, Object> map = new HashMap<>();
        map.put("oilGun", oilGun);
        map.put("pageNo", pageNo);
        return GsonUtil.GsonString(map);
    }

    public static String GetNotInvoiced(String taxNum) {
        Map<String, Object> map = new HashMap<>();
        map.put("taxNum", taxNum);
        return GsonUtil.GsonString(map);
    }
    /**
     * 异常信息上传
     *
     * @param mac
     * @param msg
     * @return
     */
    public static String ErrorMsg(String mac, String msg) {
        Map<String, Object> map = new HashMap<>();
        map.put("mac", mac);
        map.put("msg", msg);
        return GsonUtil.GsonString(map);
    }
}
