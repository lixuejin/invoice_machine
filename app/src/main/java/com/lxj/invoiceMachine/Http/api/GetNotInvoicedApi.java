package com.lxj.invoiceMachine.Http.api;

import com.hjq.http.config.IRequestApi;
//获取部分未开票信息
public class GetNotInvoicedApi implements IRequestApi {

    @Override
    public String getApi() {
        return "/jeecg-oils/oilsys/invoiceOrder/getNotInvoiced";
    }
    private String taxNum;

    public GetNotInvoicedApi setTaxNum(String taxNum) {
        this.taxNum = taxNum;
        return this;
    }
}