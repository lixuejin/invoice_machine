package com.lxj.invoiceMachine.Http.api;

import com.hjq.http.config.IRequestApi;

public class QueryOrderByOilGunaApi implements IRequestApi {

    @Override
    public String getApi() {
        return "/jeecg-oils/oilsys/orderDetails/queryOrderByOilGun";
    }

}
