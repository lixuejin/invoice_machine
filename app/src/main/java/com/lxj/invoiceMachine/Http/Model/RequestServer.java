package com.lxj.invoiceMachine.Http.Model;

import com.hjq.http.config.IRequestServer;
import com.hjq.http.model.BodyType;
import com.lxj.invoiceMachine.Base.AppConfig;

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/AndroidProject
 *    time   : 2020/10/02
 *    desc   : 服务器配置
 */
public class RequestServer implements IRequestServer {

    @Override
    public String getHost() {
        return AppConfig.GET_HOST_URL();
    }

    @Override
    public String getPath() {
        return "";
    }

    @Override
    public BodyType getType() {
        // 以表单的形式提交参数
        return BodyType.FORM;
    }
}