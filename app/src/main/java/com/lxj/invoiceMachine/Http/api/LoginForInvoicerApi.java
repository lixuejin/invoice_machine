package com.lxj.invoiceMachine.Http.api;

import com.hjq.http.config.IRequestApi;

public class LoginForInvoicerApi implements IRequestApi {

    @Override
    public String getApi() {
        return "/jeecg-oils/sys/loginForInvoicer";
    }

}
