package com.lxj.invoiceMachine.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lxj.invoiceMachine.Bean.OilGunBean;
import com.lxj.invoiceMachine.R;

import java.util.LinkedList;
import java.util.List;

public class OilGunAdapter extends BaseAdapter {

    private Context mContext;
    private List<OilGunBean> mData;

    public OilGunAdapter(List<OilGunBean> orderBeans, Context context) {
        this.mData = orderBeans;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OilGunAdapter.ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_oil_gun, parent, false);
            holder = new OilGunAdapter.ViewHolder();
            holder.tv_oil_gun = (TextView) convertView.findViewById(R.id.tv_oil_gun);
            convertView.setTag(holder);
        } else {
            holder = (OilGunAdapter.ViewHolder) convertView.getTag();
        }
        holder.tv_oil_gun.setText(mData.get(position).getGunNumber()+"号枪 "+mData.get(position).getOilType());

        return convertView;
    }

    //添加一个元素
    public void add(OilGunBean data) {
        if (mData == null) {
            mData = new LinkedList<>();
        }
        mData.add(data);
        notifyDataSetChanged();
    }

    //往特定位置，添加一个元素
    public void add(int position, OilGunBean data) {
        if (mData == null) {
            mData = new LinkedList<>();
        }
        mData.add(position, data);
        notifyDataSetChanged();
    }

    public void remove(OilGunBean data) {
        if (mData != null) {
            mData.remove(data);
        }
        notifyDataSetChanged();
    }

    public void remove(int position) {
        if (mData != null) {
            mData.remove(position);
        }
        notifyDataSetChanged();
    }

    public void clear() {
        if (mData != null) {
            mData.clear();
        }
        notifyDataSetChanged();
    }

    private class ViewHolder {
        TextView tv_oil_gun;
    }
}
