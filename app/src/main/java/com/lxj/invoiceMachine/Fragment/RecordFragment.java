package com.lxj.invoiceMachine.Fragment;

import android.graphics.Color;
import android.os.Bundle;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.kongzue.dialog.interfaces.OnInputDialogButtonClickListener;
import com.kongzue.dialog.util.BaseDialog;
import com.kongzue.dialog.v3.InputDialog;
import com.lxj.invoiceMachine.Activity.MainActivity;
import com.lxj.invoiceMachine.Base.BaseAdapter;
import com.lxj.invoiceMachine.Base.BaseFragment;
import com.lxj.invoiceMachine.Base.BaseRecyclerAdapter;
import com.lxj.invoiceMachine.Base.MyApplication;
import com.lxj.invoiceMachine.Base.RecyclerViewHolder;
import com.lxj.invoiceMachine.Bean.CommodityOrdersBean;
import com.lxj.invoiceMachine.Bean.GetNotInvoicedBean;
import com.lxj.invoiceMachine.Bean.InvoiceBean;
import com.lxj.invoiceMachine.Bean.QueryOrderByOilGunBean;
import com.lxj.invoiceMachine.Http.HttpUtil;
import com.lxj.invoiceMachine.Http.Model.HttpData;
import com.lxj.invoiceMachine.Http.api.GetInvoiceUrlApi;
import com.lxj.invoiceMachine.Http.api.GetNotInvoicedApi;
import com.lxj.invoiceMachine.Http.api.QueryOrderByOilGunaApi;
import com.lxj.invoiceMachine.R;
import com.lxj.invoiceMachine.Utils.DateUtil;
import com.lxj.invoiceMachine.Utils.InvoiceDateUtils;
import com.lxj.invoiceMachine.databinding.FragmentRecordBinding;
import com.qmuiteam.qmui.recyclerView.QMUIRVDraggableScrollBar;
import com.qmuiteam.qmui.recyclerView.QMUIRVItemSwipeAction;
import com.qmuiteam.qmui.widget.pullLayout.QMUIPullLayout;
import com.zyao89.view.zloading.ZLoadingDialog;
import com.zyao89.view.zloading.Z_TYPE;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import es.dmoral.toasty.Toasty;


public class RecordFragment extends BaseFragment<FragmentRecordBinding> {
    private List<GetNotInvoicedBean> invoiceBean = new ArrayList<>();
    private BaseRecyclerAdapter<GetNotInvoicedBean> mAdapter;
    private ZLoadingDialog zLoadingDialog;
    private int pageNo = 1;
    private int oilGun = 1;
    @Override
    protected int getContentViewId() {
        return R.layout.fragment_record;
    }

    @Override
    protected void initView() {
        binding.setOnClick(this);
        zLoadingDialog = new ZLoadingDialog(context);
        initList();
    }

    @Override
    protected void initData(Bundle savedInstanceState) {

    }
    private void initList() {

        binding.pullLayout.setActionListener(new QMUIPullLayout.ActionListener() {
            @Override
            public void onActionTriggered(@NonNull QMUIPullLayout.PullAction pullAction) {
                binding.pullLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (pullAction.getPullEdge() == QMUIPullLayout.PULL_EDGE_TOP) {
                            pageNo = 1;
                            onRefreshData(pageNo + "", oilGun + "");
                        } else if (pullAction.getPullEdge() == QMUIPullLayout.PULL_EDGE_BOTTOM) {
                            pageNo++;
                            onLoadMore(pageNo + "", oilGun + "");
                        }
                        binding.pullLayout.finishActionRun(pullAction);
                    }
                }, 3000);
            }
        });

        QMUIRVDraggableScrollBar scrollBar = new QMUIRVDraggableScrollBar(0, 0, 0);
        scrollBar.setEnableScrollBarFadeInOut(true);
        scrollBar.attachToRecyclerView(binding.recyclerView);

        QMUIRVItemSwipeAction swipeAction = new QMUIRVItemSwipeAction(true, new QMUIRVItemSwipeAction.Callback() {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                mAdapter.remove(viewHolder.getAdapterPosition());
            }

            @Override
            public int getSwipeDirection(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                return QMUIRVItemSwipeAction.SWIPE_RIGHT;
            }
        });
        swipeAction.attachToRecyclerView(binding.recyclerView);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()) {
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        });

        mAdapter = new BaseRecyclerAdapter<GetNotInvoicedBean>(getContext(), null) {
            @Override
            public int getItemLayoutId(int viewType) {
                return R.layout.item_order;
            }

            @Override
            public void bindData(RecyclerViewHolder holder, int position, GetNotInvoicedBean item) {
                holder.getTextView(R.id.tv_order_id).setText("订单号:" + item.getId());
                holder.getTextView(R.id.tv_order_time).setText("开票时间:" + item.getCreateTime());
                holder.getTextView(R.id.tv_order_money).setText(item.getOrderTotal() + "元");
            }
        };
        mAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int pos) {
                //输入开票密码，防误触
                InputDialog.show((AppCompatActivity) context, "提示", "请输入开票密码", "确定", "取消")
                        .setOnOkButtonClickListener(new OnInputDialogButtonClickListener() {
                            @Override
                            public boolean onClick(BaseDialog baseDialog, View v, String inputStr) {
                                //inputStr 即当前输入的文本
                                if (inputStr.equals("123456")){
                                    GetNotInvoicedBean bean = mAdapter.getItem(pos);
                                }
                                return false;
                            }
                        });
            }
        });
        binding.recyclerView.setAdapter(mAdapter);
        zLoadingDialog.setLoadingBuilder(Z_TYPE.DOUBLE_CIRCLE)//设置类型
                .setLoadingColor(R.color.btn_ghost_blue_border_normal)//颜色
                .setHintTextSize(14)
                .setHintText("加载中")
                .setCanceledOnTouchOutside(false)
                .setCancelable(false)
                .show();
        zLoadingDialog.show();
        onDataLoaded(pageNo + "", oilGun + "");
    }

    //初始化
    private void onDataLoaded(String oilGun, String pageNo) {

        EasyHttp.post(this)
                .api(new GetNotInvoicedApi().setTaxNum(MyApplication.loginBean.getTaxId()))
                .request(new OnHttpListener<HttpData<List<GetNotInvoicedBean>>>() {
                    @Override
                    public void onSucceed(HttpData<List<GetNotInvoicedBean>> result) {
                        zLoadingDialog.dismiss();
                        mAdapter.setData(result.getResult());
                    }

                    @Override
                    public void onFail(Exception e) {
                        zLoadingDialog.dismiss();
                        Toasty.warning(context, "请求错误:" + e.getMessage(), Toast.LENGTH_LONG, true).show();
                    }
                });

    }

    //下拉刷新
    private void onRefreshData(String oilGun, String pageNo) {

        EasyHttp.post(this)
                .api(new GetNotInvoicedApi().setTaxNum(MyApplication.loginBean.getTaxId()))
                .request(new OnHttpListener<HttpData<List<GetNotInvoicedBean>>>() {
                    @Override
                    public void onSucceed(HttpData<List<GetNotInvoicedBean>> result) {
                        mAdapter.prepend(result.getResult());
                        binding.recyclerView.scrollToPosition(0);
                        zLoadingDialog.dismiss();
                    }

                    @Override
                    public void onFail(Exception e) {
                        Toasty.warning(context, "请求错误:" + e.getMessage(), Toast.LENGTH_LONG, true).show();
                        zLoadingDialog.dismiss();
                        binding.recyclerView.scrollToPosition(0);
                    }
                });
    }

    //上拉加载
    private void onLoadMore(String oilGun, String pageNo) {
        EasyHttp.post(this)
                .api(new GetNotInvoicedApi().setTaxNum(MyApplication.loginBean.getTaxId()))
                .request(new OnHttpListener<HttpData<List<GetNotInvoicedBean>>>() {
                    @Override
                    public void onSucceed(HttpData<List<GetNotInvoicedBean>> result) {
                        mAdapter.append(result.getResult());
                    }

                    @Override
                    public void onFail(Exception e) {
                        Toasty.warning(context, "请求错误:" + e.getMessage(), Toast.LENGTH_LONG, true).show();

                    }
                });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_un_invoice:
                binding.btnUnInvoice.setBackground(getResources().getDrawable(R.drawable.btn_left_blue));
                binding.btnUnInvoice.setTextColor(Color.parseColor("#FFFFFFFF"));
                binding.btnIsOnvoice.setBackground(getResources().getDrawable(R.drawable.btn_right_white));
                binding.btnIsOnvoice.setTextColor(Color.parseColor("#FF1462df"));
                break;
            case R.id.btn_is_onvoice:
                Toasty.info(context,"暂不开放").show();

//                binding.btnUnInvoice.setBackground(getResources().getDrawable(R.drawable.btn_left_white));
//                binding.btnUnInvoice.setTextColor(Color.parseColor("#FF1462df"));
//                binding.btnIsOnvoice.setBackground(getResources().getDrawable(R.drawable.btn_right_blue));
//                binding.btnIsOnvoice.setTextColor(Color.parseColor("#FFFFFFFF"));

                break;
        }
    }

}