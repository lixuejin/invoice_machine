package com.lxj.invoiceMachine.Fragment;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnHttpListener;
import com.kongzue.dialog.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialog.interfaces.OnInputDialogButtonClickListener;
import com.kongzue.dialog.util.BaseDialog;
import com.kongzue.dialog.v3.InputDialog;
import com.kongzue.dialog.v3.MessageDialog;
import com.lxj.invoiceMachine.Activity.LoginActivity;
import com.lxj.invoiceMachine.Adapter.OilGunAdapter;
import com.lxj.invoiceMachine.Base.AppConfig;
import com.lxj.invoiceMachine.Base.BaseAdapter;
import com.lxj.invoiceMachine.Base.BaseFragment;
import com.lxj.invoiceMachine.Base.BaseRecyclerAdapter;
import com.lxj.invoiceMachine.Base.MyApplication;
import com.lxj.invoiceMachine.Base.RecyclerViewHolder;
import com.lxj.invoiceMachine.Bean.CommodityOrdersBean;
import com.lxj.invoiceMachine.Bean.CommodityPriceBean;
import com.lxj.invoiceMachine.Bean.LoginBean;
import com.lxj.invoiceMachine.Bean.OilGunBean;
import com.lxj.invoiceMachine.Bean.QueryOrderByOilGunBean;
import com.lxj.invoiceMachine.Http.HttpUtil;
import com.lxj.invoiceMachine.Http.Model.HttpData;
import com.lxj.invoiceMachine.Http.api.GetInvoiceUrlApi;
import com.lxj.invoiceMachine.Http.api.LoginForInvoicerApi;
import com.lxj.invoiceMachine.Http.api.QueryOrderByOilGunaApi;
import com.lxj.invoiceMachine.PrinterUtil.SunmiPrintHelper;
import com.lxj.invoiceMachine.R;
import com.lxj.invoiceMachine.Utils.DateUtil;
import com.lxj.invoiceMachine.Utils.InvoiceDateUtils;
import com.lxj.invoiceMachine.databinding.FragmentInvoiceBinding;
import com.lxj.invoiceMachine.view.DialogInvoice;
import com.qmuiteam.qmui.recyclerView.QMUIRVDraggableScrollBar;
import com.qmuiteam.qmui.recyclerView.QMUIRVItemSwipeAction;
import com.qmuiteam.qmui.widget.pullLayout.QMUIPullLayout;
import com.qmuiteam.qmui.widget.pullRefreshLayout.QMUIPullRefreshLayout;
import com.zyao89.view.zloading.ZLoadingDialog;
import com.zyao89.view.zloading.Z_TYPE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import es.dmoral.toasty.Toasty;

public class InvoiceFragment extends BaseFragment<FragmentInvoiceBinding> {
    private OilGunAdapter oilGunAdapter;
    private List<OilGunBean> oilGunBeans = new ArrayList<>();
    private BaseRecyclerAdapter<QueryOrderByOilGunBean> mAdapter;
    private ZLoadingDialog zLoadingDialog;
    private int pageNo = 1;
    private OilGunBean oilGunBean;

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_invoice;
    }

    @Override
    protected void initView() {
        binding.setOnClick(this);
        zLoadingDialog = new ZLoadingDialog(context);
        oilGunBeans = MyApplication.loginBean.getOilGunList();
        if (oilGunBeans == null) {
            return;
        }
        pageNo = 1;
        oilGunBean = oilGunBeans.get(0);
        binding.tvOrderList.setText("订单列表  " + oilGunBean.getGunNumber() + "号枪 " + oilGunBeans.get(0).getOilType());
        //上面的油枪列表
        oilGunAdapter = new OilGunAdapter(oilGunBeans, getContext());
        binding.gvOilGun.setAdapter(oilGunAdapter);
        binding.gvOilGun.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //点击获取到枪号
                zLoadingDialog.setLoadingBuilder(Z_TYPE.DOUBLE_CIRCLE)//设置类型
                        .setLoadingColor(R.color.qmui_config_color_blue)//颜色
                        .setHintTextSize(14)
                        .setHintText("加载中")
                        .setCanceledOnTouchOutside(false)
                        .setCancelable(false)
                        .show();
                zLoadingDialog.show();
                oilGunBean = oilGunBeans.get(position);
                binding.tvOrderList.setText("订单列表  " + oilGunBeans.get(position).getGunNumber() + "号枪 " + oilGunBeans.get(position).getOilType());
                onRefreshData(pageNo + "", oilGunBeans.get(position).getGunNumber() + "");
            }
        });
        //下面的列表
        initList();

    }

    @Override
    protected void initData(Bundle savedInstanceState) {

    }

    private void initList() {

        binding.pullLayout.setActionListener(new QMUIPullLayout.ActionListener() {
            @Override
            public void onActionTriggered(@NonNull QMUIPullLayout.PullAction pullAction) {
                binding.pullLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (pullAction.getPullEdge() == QMUIPullLayout.PULL_EDGE_TOP) {
                            pageNo = 1;
                            onRefreshData(pageNo + "", oilGunBean.getGunNumber() + "");
                        } else if (pullAction.getPullEdge() == QMUIPullLayout.PULL_EDGE_BOTTOM) {
                            pageNo++;
                            onLoadMore(pageNo + "", oilGunBean.getGunNumber() + "");
                        }
                        binding.pullLayout.finishActionRun(pullAction);
                    }
                }, 3000);
            }
        });

        QMUIRVDraggableScrollBar scrollBar = new QMUIRVDraggableScrollBar(0, 0, 0);
        scrollBar.setEnableScrollBarFadeInOut(true);
        scrollBar.attachToRecyclerView(binding.recyclerView);

        QMUIRVItemSwipeAction swipeAction = new QMUIRVItemSwipeAction(true, new QMUIRVItemSwipeAction.Callback() {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                mAdapter.remove(viewHolder.getAdapterPosition());
            }

            @Override
            public int getSwipeDirection(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                return QMUIRVItemSwipeAction.SWIPE_RIGHT;
            }
        });
        swipeAction.attachToRecyclerView(binding.recyclerView);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()) {
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        });

        mAdapter = new BaseRecyclerAdapter<QueryOrderByOilGunBean>(getContext(), null) {
            @Override
            public int getItemLayoutId(int viewType) {
                return R.layout.item_order;
            }

            @Override
            public void bindData(RecyclerViewHolder holder, int position, QueryOrderByOilGunBean item) {
                holder.getTextView(R.id.tv_order_id).setText("订单号:" + item.getId());
                holder.getTextView(R.id.tv_order_time).setText("时间:" + item.getCreateTime());
                holder.getTextView(R.id.tv_order_money).setText(item.getPayAmt() + "元");
            }
        };
        mAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int pos) {
                MessageDialog.show((AppCompatActivity) context, "提示", "确认开票吗?", "确认", "取消")
                        .setOnOkButtonClickListener(new OnDialogButtonClickListener() {
                            @Override
                            public boolean onClick(BaseDialog baseDialog, View v) {
                                QueryOrderByOilGunBean bean = mAdapter.getItem(pos);
                                List<CommodityPriceBean> commodityPriceBeans = MyApplication.loginBean.getCommodityList();
                                CommodityPriceBean commodityPriceBean = new CommodityPriceBean();
                                for (int i = 0; i < commodityPriceBeans.size(); i++) {
                                    if (commodityPriceBeans.get(i).getCommodityName().equals(oilGunBean.getOilType())) {
                                        commodityPriceBean = commodityPriceBeans.get(i);
                                        break;
                                    }
                                }
                                Incoice(bean.getOilNum(), bean.getOilPrice(), bean.getPayAmt(), commodityPriceBean.getTaxRate()
                                        , commodityPriceBean.getCommodityUnit(), commodityPriceBean.getCommodityInvoiceName());
                                return false;
                            }
                        });
            }
        });
        binding.recyclerView.setAdapter(mAdapter);
        zLoadingDialog.setLoadingBuilder(Z_TYPE.DOUBLE_CIRCLE)//设置类型
                .setLoadingColor(R.color.btn_ghost_blue_border_normal)//颜色
                .setHintTextSize(14)
                .setHintText("加载中")
                .setCanceledOnTouchOutside(false)
                .setCancelable(false)
                .show();
        zLoadingDialog.show();
        onDataLoaded(pageNo + "", oilGunBean.getGunNumber() + "");
    }

    private void Incoice(String num, String price, String orderTotal, float taxrate, String unit
            , String InvoiceName) {

        /**
         * 成品油
         */
        CommodityOrdersBean commodityOrdersBean = new CommodityOrdersBean(InvoiceName, num, "1", price, taxrate + ""
                , "", unit, "", "", "", "", "", "", "", "", "");

        List<CommodityOrdersBean> commodityOrdersBeans = new ArrayList<>();
        commodityOrdersBeans.add(commodityOrdersBean);
        //合计税额
        float taxTotal = Float.parseFloat(price) * taxrate;
        EasyHttp.post(this)
                .json(HttpUtil.getInvoiceUrl(InvoiceDateUtils.orderNo(), orderTotal, taxTotal + "", "0"
                        , "0", "发票", DateUtil.parseDateToStr(new Date(), DateUtil.DATE_TIME_FORMAT_YYYY_MM_DD_HH_MI_SS), "", "admin"
                        , "admin", "admin", "1", "1", "1", commodityOrdersBeans, MyApplication.loginBean.getTaxId()))
                .api(new GetInvoiceUrlApi())
                .request(new OnHttpListener<HttpData<String>>() {
                    @Override
                    public void onSucceed(HttpData<String> result) {
                        zLoadingDialog.dismiss();
                        Invoice(InvoiceName, num, price, orderTotal, result.getResult());
                    }

                    @Override
                    public void onFail(Exception e) {
                        zLoadingDialog.dismiss();
                        Toasty.warning(context, "开票失败:" + e.getMessage(), Toast.LENGTH_LONG, true).show();

                    }
                });
//        /**
//         * 非成品油
//         */
//        CommodityOrdersBean commodityOrdersBean = new CommodityOrdersBean("酸梅汤", num, "1", price, taxrate+""
//                , "", "升", "", "", "", "", "", "", "", "", "");
//        List<CommodityOrdersBean> commodityOrdersBeans = new ArrayList<>();
//        commodityOrdersBeans.add(commodityOrdersBean);
//        //合计税额
//        float taxTotal =  Float.parseFloat(price)*taxrate;
//        EasyHttp.post(this)
//                .json(HttpUtil.getInvoiceUrl(InvoiceDateUtils.orderNo(), orderTotal, taxTotal+"", "0"
//                        , "0", "11", DateUtil.parseDateToStr(new Date(), DateUtil.DATE_TIME_FORMAT_YYYY_MM_DD_HH_MI_SS), "", "admin"
//                        , "admin", "admin", "1", "1", "0", commodityOrdersBeans,MyApplication.loginBean.getTaxId()))
//                .api(new GetInvoiceUrlApi())
//                .request(new OnHttpListener<HttpData<String>>() {
//                    @Override
//                    public void onSucceed(HttpData<String> result) {
//                        zLoadingDialog.dismiss();
////                        Invoice(goodsname,num,price,orderTotal,result.getResult());
//                    }
//
//                    @Override
//                    public void onFail(Exception e) {
//                        zLoadingDialog.dismiss();
//                        Toasty.warning(context, "开票失败:" + e.getMessage(), Toast.LENGTH_LONG, true).show();
//
//                    }
//                });
    }

    //初始化
    private void onDataLoaded(String pageNo, String oilGun) {

        EasyHttp.post(this)
                .json(HttpUtil.QueryOrderByOilGun(oilGun, pageNo))
                .api(new QueryOrderByOilGunaApi())
                .request(new OnHttpListener<HttpData<List<QueryOrderByOilGunBean>>>() {
                    @Override
                    public void onSucceed(HttpData<List<QueryOrderByOilGunBean>> result) {
                        zLoadingDialog.dismiss();
                        mAdapter.setData(result.getResult());
                    }

                    @Override
                    public void onFail(Exception e) {
                        zLoadingDialog.dismiss();
                        Toasty.warning(context, "请求错误:" + e.getMessage(), Toast.LENGTH_LONG, true).show();
                    }
                });

    }

    //下拉刷新
    private void onRefreshData(String pageNo, String oilGun) {

        EasyHttp.post(this)
                .json(HttpUtil.QueryOrderByOilGun(oilGun, pageNo))
                .api(new QueryOrderByOilGunaApi())
                .request(new OnHttpListener<HttpData<List<QueryOrderByOilGunBean>>>() {
                    @Override
                    public void onSucceed(HttpData<List<QueryOrderByOilGunBean>> result) {
                        mAdapter.prepend(result.getResult());
                        binding.recyclerView.scrollToPosition(0);
                        zLoadingDialog.dismiss();
                    }

                    @Override
                    public void onFail(Exception e) {
                        Toasty.warning(context, "请求错误:" + e.getMessage(), Toast.LENGTH_LONG, true).show();
                        zLoadingDialog.dismiss();
                        binding.recyclerView.scrollToPosition(0);
                    }
                });
    }

    //上拉加载
    private void onLoadMore(String pageNo, String oilGun) {
        EasyHttp.post(this)
                .json(HttpUtil.QueryOrderByOilGun(oilGun, pageNo))
                .api(new QueryOrderByOilGunaApi())
                .request(new OnHttpListener<HttpData<List<QueryOrderByOilGunBean>>>() {
                    @Override
                    public void onSucceed(HttpData<List<QueryOrderByOilGunBean>> result) {
                        mAdapter.append(result.getResult());
                    }

                    @Override
                    public void onFail(Exception e) {
                        Toasty.warning(context, "请求错误:" + e.getMessage(), Toast.LENGTH_LONG, true).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cl_money_invoice:
                DialogInvoice dialogInvoice = new DialogInvoice();
                dialogInvoice.setBack(new DialogInvoice.InvoiceBack() {
                    @Override
                    public void InvoiceBack(String money, CommodityPriceBean bean, String num) {
                        Incoice(num, bean.getCommodityPrice() + "", money, bean.getTaxRate(), bean.getCommodityUnit()
                                , bean.getCommodityInvoiceName());
                    }
                });
                dialogInvoice.show(getFragmentManager());
                break;
        }
    }

    private void Invoice(String goodsname, String num, String price, String orderTotal, String Qr) {
        SunmiPrintHelper.getInstance().setAlign(0);
        SunmiPrintHelper.getInstance().printText("商户名称:" + MyApplication.loginBean.getBusinessName() + "\n" +
                "商户税号:" + MyApplication.loginBean.getTaxId() + "\n" +
                "商户电话:" + MyApplication.loginBean.getBusinessPhone() + "\n" +
                "————————————————\n" +
                "交易类型:现金支付\n" +
                "时间：" + DateUtil.parseDateToStr(new Date(), DateUtil.DATE_TIME_FORMAT_YYYY_MM_DD_HH_MI_SS) + "\n" +
                "", 24, true, false);
        SunmiPrintHelper.getInstance().printText("请用微信扫描二维码申请发票\n" +
                        "油品类型:" + goodsname + "\n" + "数量:" + num + "L\n" +
                        "单价:" + price + "元/L\n总价:" + orderTotal + "元\n"
                , 24, true, false);
        SunmiPrintHelper.getInstance().setAlign(1);
        SunmiPrintHelper.getInstance().printQr(Qr, 6, 3);
        SunmiPrintHelper.getInstance().feedPaper();
        SunmiPrintHelper.getInstance().print3Line();
    }
}