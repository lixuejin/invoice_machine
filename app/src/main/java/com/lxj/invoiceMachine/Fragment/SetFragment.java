package com.lxj.invoiceMachine.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kongzue.dialog.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialog.util.BaseDialog;
import com.kongzue.dialog.v3.MessageDialog;
import com.lxj.invoiceMachine.Activity.LoginActivity;
import com.lxj.invoiceMachine.Base.AppConfig;
import com.lxj.invoiceMachine.Base.BaseFragment;
import com.lxj.invoiceMachine.Base.MyApplication;
import com.lxj.invoiceMachine.R;
import com.lxj.invoiceMachine.databinding.FragmentSetBinding;

public class SetFragment extends BaseFragment<FragmentSetBinding> {

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_set;
    }

    @Override
    protected void initView() {
        binding.setOnClick(this);
        binding.tvRealName.setText(MyApplication.loginBean.getRealName());
    }

    @Override
    protected void initData(Bundle savedInstanceState) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cl_login_out:
                //一次性完成所有参数预设操作
                MessageDialog.show((AppCompatActivity) context, "提示", "确认退出登录吗?", "确认", "取消")
                        .setOnOkButtonClickListener(new OnDialogButtonClickListener() {
                            @Override
                            public boolean onClick(BaseDialog baseDialog, View v) {
                                MyApplication.sharedPreferences.put(AppConfig.PassWord,"");
                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                                return false;
                            }
                        });
                break;
        }
    }
}