package com.lxj.invoiceMachine.view;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.lxj.invoiceMachine.Base.MyApplication;
import com.lxj.invoiceMachine.Bean.CommodityPriceBean;
import com.lxj.invoiceMachine.Bean.OilGunBean;
import com.lxj.invoiceMachine.R;

import org.angmarch.views.NiceSpinner;
import org.angmarch.views.OnSpinnerItemSelectedListener;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import es.dmoral.toasty.Toasty;

public class DialogInvoice extends DialogFragment {
    private InvoiceBack invoiceBack;
    private CommodityPriceBean commodityPriceBean;

    public void setBack(InvoiceBack invoiceBack) {
        this.invoiceBack = invoiceBack;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_invoice, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //设置弹框位置
//        Dialog dialog = getDialog();
//        dialog.getWindow().setGravity(Gravity.BOTTOM);
        //输入框
        EditText etMoney = view.findViewById(R.id.et_money);
        //选择框
        NiceSpinner niceSpinner = view.findViewById(R.id.sp_price);
        TextView tvNum = view.findViewById(R.id.tv_num);
        tvNum.setText("0.0L");
        //选择框逻辑
        List<CommodityPriceBean> commodityPriceBeans = MyApplication.loginBean.getCommodityList();
        if (commodityPriceBeans == null || commodityPriceBeans.size() == 0) {
            return;
        }
        commodityPriceBean = commodityPriceBeans.get(0);
        List<String> oilTpye = new ArrayList<>();
        for (int i = 0; i < commodityPriceBeans.size(); i++) {
            oilTpye.add(commodityPriceBeans.get(i).getCommodityName() + " " + commodityPriceBeans.get(i).getCommodityPrice() + "元/L");
        }
        niceSpinner.attachDataSource(oilTpye);
        niceSpinner.setOnSpinnerItemSelectedListener(new OnSpinnerItemSelectedListener() {
            @Override
            public void onItemSelected(NiceSpinner parent, View view, int position, long id) {
                // This example uses String, but your type can be any
                String item = (String) parent.getItemAtPosition(position);
                commodityPriceBean = commodityPriceBeans.get(position);

                String money = etMoney.getText().toString();
                float money_double = 0;
                if (money.equals("")) {
                    money_double = 0;
                } else {
                    money_double = Float.parseFloat(money);
                }
                BigDecimal b  =   new BigDecimal(money_double / commodityPriceBean.getCommodityPrice());
                float   f1   =  b.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
                tvNum.setText(f1 + "L");
            }
        });
        //输入框逻辑
        etMoney.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                float money_double = 0;
                String money = etMoney.getText().toString();
                if (money.equals("")) {
                    money_double = 0;
                } else {
                    money_double = Float.parseFloat(money);
                }
                BigDecimal b  =   new BigDecimal(money_double / commodityPriceBean.getCommodityPrice());
                float   f1   =  b.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
                tvNum.setText(f1 + "L");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //确认键
        view.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etMoney.getText().toString().equals("")){
                    Toasty.warning(getContext(),"未输入金额").show();
                    return;
                }
                dismissAllowingStateLoss();
                String num = tvNum.getText().toString();
                // 目标：删除最后一个 ","
                num = num.substring(0, num.length() - 1);

                invoiceBack.InvoiceBack(etMoney.getText().toString(), commodityPriceBean, num);

            }
        });
        view.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllowingStateLoss();
            }
        });

    }

    public void show(FragmentManager manager) {
        super.show(manager, getTag());
    }

    public interface InvoiceBack {
        void InvoiceBack(String money, CommodityPriceBean bean, String num);
    }
}
