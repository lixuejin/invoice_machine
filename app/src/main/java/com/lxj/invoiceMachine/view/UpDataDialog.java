package com.lxj.invoiceMachine.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.hjq.http.EasyHttp;
import com.hjq.http.listener.OnDownloadListener;
import com.hjq.http.model.HttpMethod;
import com.lxj.invoiceMachine.R;
import com.orhanobut.logger.Logger;
import com.timmy.tdialog.TDialog;
import com.timmy.tdialog.base.BindViewHolder;
import com.timmy.tdialog.listener.OnBindViewListener;
import com.timmy.tdialog.listener.OnViewClickListener;

import java.io.File;

import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import es.dmoral.toasty.Toasty;

public class UpDataDialog {
    public static void mustUp(FragmentManager fragmentManager, Context context, String msg, String downFile, LifecycleOwner lifecycleOwner) {
        new TDialog.Builder(fragmentManager)
                .setLayoutRes(R.layout.dialog_version_upgrde_strong)
                .setScreenWidthAspect(context, 0.7f)
                .addOnClickListener(R.id.tv_confirm)
                .setCancelableOutside(false)
                .setDialogAnimationRes(R.style.animate_dialog_scale)
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            Toast.makeText(context, "返回健无效，请强制升级后使用", Toast.LENGTH_SHORT).show();
                            return true;
                        }
                        return false;
                    }
                })
                .setOnViewClickListener(new OnViewClickListener() {
                    @Override
                    public void onViewClick(BindViewHolder viewHolder, View view, TDialog tDialog) {
                        Toasty.info(context, R.string.downapk, Toast.LENGTH_SHORT, true).show();
                        downApk(downFile, lifecycleOwner, context);
                    }
                })
                .setOnBindViewListener(new OnBindViewListener() {
                    @Override
                    public void bindView(BindViewHolder bindViewHolder) {
                        bindViewHolder.setText(R.id.tv_upgrade_content, msg);
                    }
                })
                .create()
                .show();
    }

    public static void NeedUp(FragmentManager fragmentManager, Context context, String msg, String downFile, LifecycleOwner lifecycleOwner) {
        new TDialog.Builder(fragmentManager)
                .setLayoutRes(R.layout.dialog_version_upgrde)
                .setScreenWidthAspect(context, 0.7f)
                .addOnClickListener(R.id.tv_cancel, R.id.tv_confirm)
                .setDialogAnimationRes(R.style.animate_dialog_scale)
                .setOnViewClickListener(new OnViewClickListener() {
                    @Override
                    public void onViewClick(BindViewHolder viewHolder, View view, TDialog tDialog) {
                        switch (view.getId()) {
                            case R.id.tv_cancel:
                                tDialog.dismiss();
                                break;
                            case R.id.tv_confirm:
                                Toasty.info(context, R.string.downapk, Toast.LENGTH_SHORT, true).show();
                                downApk(downFile, lifecycleOwner, context);
                                break;
                        }
                    }
                })
                .setOnBindViewListener(new OnBindViewListener() {
                    @Override
                    public void bindView(BindViewHolder bindViewHolder) {

                        bindViewHolder.setText(R.id.tv_upgrade_content, "" + msg);
                    }
                })
                .create()
                .show();
    }

    private static void downApk(String downFile, LifecycleOwner lifecycleOwner, Context context) {
        Logger.d("下载路径" + downFile);
        EasyHttp.download(lifecycleOwner)
                .method(HttpMethod.GET)
                .file(new File(Environment.getExternalStorageDirectory(), "智慧加油机.apk"))
                .url(downFile)
                .listener(new OnDownloadListener() {
                    @Override
                    public void onStart(File file) {
                        Logger.d("开始下载");
                    }

                    @Override
                    public void onProgress(File file, int progress) {
                        Logger.d("正在下载");
                    }

                    @Override
                    public void onComplete(File file) {
                        Logger.d("完成下载" + file.getPath());
                        //安装

                        if (!file.exists()) {
                            return;
                        }
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        //安装完成后，启动app
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".fileprovider", file);//第二个参数要和Mainfest中<provider>内的android:authorities 保持一致
                            intent.setDataAndType(uri, "application/vnd.android.package-archive");
                        } else {
                            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
                        }
                        context.startActivity(intent);

                    }

                    @Override
                    public void onError(File file, Exception e) {
                        Logger.d("下载错误" + e.getMessage());
                    }

                    @Override
                    public void onEnd(File file) {
                        Logger.d("下载结束");
                    }
                }).start();
    }

}
