package com.lxj.invoiceMachine.Base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;


/**
 * Created by leo
 * on 2019/11/27.
 */
public abstract class BaseFragment< VDB extends ViewDataBinding> extends Fragment implements View.OnClickListener {
    //获取当前activity布局文件
    protected abstract int getContentViewId();
    //初始化布局
    protected abstract void initView();
    //处理逻辑业务
    protected abstract void initData(Bundle savedInstanceState);

    protected View mContentView;
    protected VDB binding;
    protected Context context;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        // 避免多次从xml中加载布局文件
        if (mContentView == null) {
            binding = DataBindingUtil.inflate(inflater, getContentViewId(), null, false);
            mContentView = binding.getRoot();
            binding.setLifecycleOwner(this);
            context = getContext();
            initView();
            initData(savedInstanceState);
        } else {
            ViewGroup parent = (ViewGroup) mContentView.getParent();
            if (parent != null) {
                parent.removeView(mContentView);
            }
        }
        return mContentView;
    }

}
