package com.lxj.invoiceMachine.Base;

import android.app.Application;
import android.content.Context;

import com.hjq.http.EasyConfig;
import com.kongzue.dialog.util.DialogSettings;

import com.lxj.invoiceMachine.Bean.LoginBean;
import com.lxj.invoiceMachine.Http.Model.RequestHandler;
import com.lxj.invoiceMachine.Http.Model.RequestServer;
import com.lxj.invoiceMachine.PrinterUtil.SunmiPrintHelper;
import com.lxj.invoiceMachine.Utils.CrashHandler;
import com.lxj.invoiceMachine.Utils.SharedPreferencesHelper;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import es.dmoral.toasty.Toasty;
import okhttp3.OkHttpClient;


//import okhttp3.OkHttpClient;OkHttpClient

/**
 * Created by leo
 * on 2019/10/15.
 */
public class MyApplication extends Application {


    public static LoginBean loginBean;
    private static MyApplication context;
    public static SharedPreferencesHelper sharedPreferences;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //捕获崩溃日志，位置在外部存储的LianSou
        context = this;
        init();

    }

    public static Context getContext() {
        return context;
    }

    private void init() {
        // 本地异常捕捉
//        CrashHandler crashHandler = CrashHandler.getInstance();
//        crashHandler.init(context);
        //打印机初始化
        SunmiPrintHelper.getInstance().initSunmiPrinterService(this);

        // 网络请求框架初始化
        initHttp();
        //log初始化
        Logger.addLogAdapter(new AndroidLogAdapter());
        //初始化存储
        sharedPreferences = new SharedPreferencesHelper(getContext(), AppConfig.SET);
        //toast初始化
        Toasty.Config.reset();
        //初始化更新
        initUpData();
        //对话框初始化
        DialogSettings.style = DialogSettings.STYLE.STYLE_KONGZUE;          //全局主题风格，提供三种可选风格，STYLE_MATERIAL, STYLE_KONGZUE, STYLE_IOS
    }

    private void initUpData() {

    }

    private void initHttp() {
        // 网络请求框架初始化
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .build();
        EasyConfig.with(okHttpClient)
                // 是否打印日志
                .setLogEnabled(AppConfig.debug())
                // 设置服务器配置
                .setServer(new RequestServer())
                // 设置请求处理策略
                .setHandler(new RequestHandler(context))
                // 设置请求重试次数
                .setRetryCount(3)
                .setRetryTime(30000)
                // 添加全局请求参数
                //.addParam("token", "6666666")
                // 添加全局请求头
                //.addHeader("time", "20191030")
                // 启用配置
                .into();

    }

}
