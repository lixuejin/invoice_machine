package com.lxj.invoiceMachine.Base;

public class AppConfig {
    public static final Boolean debug = true;
    public static final String AESKEY = "1234567891011123";
    //测试
//    public static String HOST_URL_TEST = "192.168.50.221:8080";
//    http://101.37.172.227:60800/jeecg-oils/
    public static String HOST_URL_TEST = "101.37.172.227:60800";
//    public static String HOST_URL_TEST = "192.168.0.105:8080";

    //正式
    public static String HOST_URL = "101.37.172.227:60800";


    //广告下载地址
    public static String title = "智慧加油站";

    //获取完整URL
    public static String GET_HOST_URL() {
        if (debug) {
            return "http://" + HOST_URL_TEST + "/";
        } else {
            return "http://" + HOST_URL + "/";
        }
    }

    //本地存储设置
    public static final String UserName = "UserName";//本地存储-设置
    public static final String PassWord = "PassWord";//本地存储-密码
    public static final String AdSave = "AdSave";//本地存储-广告
    public static final String SET = "set";//本地存储-设置
    public static final String SET_IP = "set_ip";//本地存储-IP
    public static String loginBean = "loginBean";
    public static boolean debug() {
        return debug;
    }

    //下半个屏的fragment
    public static final int pageCharge = 0;//计费
    public static final int pageRefuel = 1;//加油
    public static final int pageSet = 2;//设置
    public static final int pagePay = 3;//支付
    public static final int pageCar = 4;//选择车辆
    public static final int pageIsPay = 5;//支付
    public static final int pageUnPay = 6;//选择车辆
    public static final int pageLogin = 7;//登录
    //上半个屏的fragment
    public static final int Page_AdVideo = 0;
    public static final int Page_AdPicture = 1;
    //支付方式
    public static final int PAY_ETC = 0;//ETC支付
    public static final int PAY_CAR_NUM = 1;//车牌付
    public static final int PAY_ALI = 2;//支付宝支付
    public static final int PAY_WECHAT = 3;//微信支付
    public static final int PAY_IC = 4;//IC卡支付
    //
    public static final int onTransData = 101;
    public static final int onGetRealData = 102;
}
