package com.lxj.invoiceMachine.Bean;

public class QueryOrderByOilGunBean {
    private String gunNo;
    private String id;
    private String payAmt;
    private String oilPrice;
    private String oilDetail;
    private String createTime;
    private String oilNum;
    private String orderInvoiceNo;

    public String getOrderInvoiceNo() {
        return orderInvoiceNo;
    }

    public void setOrderInvoiceNo(String orderInvoiceNo) {
        this.orderInvoiceNo = orderInvoiceNo;
    }

    public void setGunNo(String gunNo) {
        this.gunNo = gunNo;
    }
    public String getGunNo() {
        return gunNo;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }

    public void setPayAmt(String payAmt) {
        this.payAmt = payAmt;
    }
    public String getPayAmt() {
        return payAmt;
    }

    public void setOilPrice(String oilPrice) {
        this.oilPrice = oilPrice;
    }
    public String getOilPrice() {
        return oilPrice;
    }

    public void setOilDetail(String oilDetail) {
        this.oilDetail = oilDetail;
    }
    public String getOilDetail() {
        return oilDetail;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    public String getCreateTime() {
        return createTime;
    }

    public void setOilNum(String oilNum) {
        this.oilNum = oilNum;
    }
    public String getOilNum() {
        return oilNum;
    }
}
