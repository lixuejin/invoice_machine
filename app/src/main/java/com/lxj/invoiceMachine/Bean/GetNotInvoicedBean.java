package com.lxj.invoiceMachine.Bean;

public class GetNotInvoicedBean {

    private String id;
    private String createBy;
    private String createTime;
    private String updateBy;
    private String updateTime;
    private String orderNo;
    private int orderTotal;
    private int taxTotal;
    private int bhtaxTotal;
    private String qdbz;
    private String qdxmmc;
    private String invoicedate;
    private String message;
    private String clerk;
    private String payee;
    private String checker;
    private String deptid;
    private String clerkid;
    private String cpybz;
    private String kptype;
    private String taxNum;
    private String fpdm;
    private String fphm;
    private String fpqqlsh;
    private String url;
    private String jpgUrl;
    private String email;
    private String phone;
    private String buyTaxNum;
    private String buyername;
    private String telephone;
    private String address;
    private String bankAccount;
    private String checkCode;
    private String qrCode;
    private String machineCode;
    private String cipherText;
    private String invoiceLine;
    private String salerAccount;
    private String salerTel;
    private String salerAddress;
    private String extensionNumber;
    private String imgUrls;
    private String paperPdfUrl;
    private String ofdUrl;
    private String kprq;
    private String isInvoice;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderTotal(int orderTotal) {
        this.orderTotal = orderTotal;
    }

    public int getOrderTotal() {
        return orderTotal;
    }

    public void setTaxTotal(int taxTotal) {
        this.taxTotal = taxTotal;
    }

    public int getTaxTotal() {
        return taxTotal;
    }

    public void setBhtaxTotal(int bhtaxTotal) {
        this.bhtaxTotal = bhtaxTotal;
    }

    public int getBhtaxTotal() {
        return bhtaxTotal;
    }

    public void setQdbz(String qdbz) {
        this.qdbz = qdbz;
    }

    public String getQdbz() {
        return qdbz;
    }

    public void setQdxmmc(String qdxmmc) {
        this.qdxmmc = qdxmmc;
    }

    public String getQdxmmc() {
        return qdxmmc;
    }

    public void setInvoicedate(String invoicedate) {
        this.invoicedate = invoicedate;
    }

    public String getInvoicedate() {
        return invoicedate;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setClerk(String clerk) {
        this.clerk = clerk;
    }

    public String getClerk() {
        return clerk;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public String getPayee() {
        return payee;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    public String getChecker() {
        return checker;
    }

    public void setDeptid(String deptid) {
        this.deptid = deptid;
    }

    public String getDeptid() {
        return deptid;
    }

    public void setClerkid(String clerkid) {
        this.clerkid = clerkid;
    }

    public String getClerkid() {
        return clerkid;
    }

    public void setCpybz(String cpybz) {
        this.cpybz = cpybz;
    }

    public String getCpybz() {
        return cpybz;
    }

    public void setKptype(String kptype) {
        this.kptype = kptype;
    }

    public String getKptype() {
        return kptype;
    }

    public void setTaxNum(String taxNum) {
        this.taxNum = taxNum;
    }

    public String getTaxNum() {
        return taxNum;
    }

    public void setFpdm(String fpdm) {
        this.fpdm = fpdm;
    }

    public String getFpdm() {
        return fpdm;
    }

    public void setFphm(String fphm) {
        this.fphm = fphm;
    }

    public String getFphm() {
        return fphm;
    }

    public void setFpqqlsh(String fpqqlsh) {
        this.fpqqlsh = fpqqlsh;
    }

    public String getFpqqlsh() {
        return fpqqlsh;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setJpgUrl(String jpgUrl) {
        this.jpgUrl = jpgUrl;
    }

    public String getJpgUrl() {
        return jpgUrl;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setBuyTaxNum(String buyTaxNum) {
        this.buyTaxNum = buyTaxNum;
    }

    public String getBuyTaxNum() {
        return buyTaxNum;
    }

    public void setBuyername(String buyername) {
        this.buyername = buyername;
    }

    public String getBuyername() {
        return buyername;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setCheckCode(String checkCode) {
        this.checkCode = checkCode;
    }

    public String getCheckCode() {
        return checkCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setMachineCode(String machineCode) {
        this.machineCode = machineCode;
    }

    public String getMachineCode() {
        return machineCode;
    }

    public void setCipherText(String cipherText) {
        this.cipherText = cipherText;
    }

    public String getCipherText() {
        return cipherText;
    }

    public void setInvoiceLine(String invoiceLine) {
        this.invoiceLine = invoiceLine;
    }

    public String getInvoiceLine() {
        return invoiceLine;
    }

    public void setSalerAccount(String salerAccount) {
        this.salerAccount = salerAccount;
    }

    public String getSalerAccount() {
        return salerAccount;
    }

    public void setSalerTel(String salerTel) {
        this.salerTel = salerTel;
    }

    public String getSalerTel() {
        return salerTel;
    }

    public void setSalerAddress(String salerAddress) {
        this.salerAddress = salerAddress;
    }

    public String getSalerAddress() {
        return salerAddress;
    }

    public void setExtensionNumber(String extensionNumber) {
        this.extensionNumber = extensionNumber;
    }

    public String getExtensionNumber() {
        return extensionNumber;
    }

    public void setImgUrls(String imgUrls) {
        this.imgUrls = imgUrls;
    }

    public String getImgUrls() {
        return imgUrls;
    }

    public void setPaperPdfUrl(String paperPdfUrl) {
        this.paperPdfUrl = paperPdfUrl;
    }

    public String getPaperPdfUrl() {
        return paperPdfUrl;
    }

    public void setOfdUrl(String ofdUrl) {
        this.ofdUrl = ofdUrl;
    }

    public String getOfdUrl() {
        return ofdUrl;
    }

    public void setKprq(String kprq) {
        this.kprq = kprq;
    }

    public String getKprq() {
        return kprq;
    }

    public void setIsInvoice(String isInvoice) {
        this.isInvoice = isInvoice;
    }

    public String getIsInvoice() {
        return isInvoice;
    }

}

