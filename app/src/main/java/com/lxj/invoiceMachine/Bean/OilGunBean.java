package com.lxj.invoiceMachine.Bean;

public class OilGunBean {
    private int gunNumber;//枪号
    private String oilType;//油品类型

    public OilGunBean(int GunNumber, String OilType) {
        this.gunNumber = GunNumber;
        this.oilType = OilType;
    }

    public int getGunNumber() {
        return gunNumber;
    }

    public void setGunNumber(int gunNumber) {
        gunNumber = gunNumber;
    }

    public String getOilType() {
        return oilType;
    }

    public void setOilType(String oilType) {
        oilType = oilType;
    }


}
