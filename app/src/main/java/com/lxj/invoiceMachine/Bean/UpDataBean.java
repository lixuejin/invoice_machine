package com.lxj.invoiceMachine.Bean;

public class UpDataBean {
    /**
     * 下载地址
     */
    private String apkFile;
    /**
     * 是否强制升级 0非强制，1强制
     */
    private int apkMust;
    /**
     *版本号
     */
    private int apkVersion;
    /**
     *版本信息
     */
    private String apkMessage;

    public String getApkFile() {
        return apkFile;
    }

    public void setApkFile(String apkFile) {
        this.apkFile = apkFile;
    }

    public int getApkMust() {
        return apkMust;
    }

    public void setApkMust(int apkMust) {
        this.apkMust = apkMust;
    }

    public int getApkVersion() {
        return apkVersion;
    }

    public void setApkVersion(int apkVersion) {
        this.apkVersion = apkVersion;
    }

    public String getApkMessage() {
        return apkMessage;
    }

    public void setApkMessage(String apkMessage) {
        this.apkMessage = apkMessage;
    }
}
