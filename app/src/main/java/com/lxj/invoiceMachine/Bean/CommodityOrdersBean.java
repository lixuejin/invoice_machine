package com.lxj.invoiceMachine.Bean;

public class CommodityOrdersBean {
    /**
     * 商品名称
     */
    private String goodsname;
    /**
     * 数量
     */
    private String num;
    /**
     * 单价含税标志
     */
    private String hsbz;
    /**
     * 单价
     */
    private String price;
    /**
     * 税率
     */
    private String taxrate;
    /**
     * 规格型号
     */
    private String spec;
    /**
     * 单位
     */
    private String unit;
    /**
     * 商品编码
     */
    private String spbm;
    /**
     * 自行编码
     */
    private String zsbm;
    /**
     * 发票行性质:0,正常行(默 认);1,折扣行;2,被折扣行
     */
    private String fphxz;
    /**
     * 优惠政策标识 :0, 不 使用;1,使用
     */
    private String yhzcbs;
    /**
     * 增值税特殊管理（优惠政策名称）
     */
    private String zzstsgl;
    /**
     * 零 税 率 标 识 : 空 , 非 零 税率;1,免税;2,不征税;3,普通零税率
     */
    private String lslbs;
    /**
     * 不含税金额
     */
    private String taxfreeamt;
    /**
     * 税额
     */
    private String tax;
    /**
     * 含税金额
     */
    private String taxamt;

    public CommodityOrdersBean(String goodsname, String num, String hsbz, String price, String taxrate,
                               String spec, String unit, String spbm, String zsbm, String fphxz,
                               String yhzcbs, String zzstsgl, String lslbs, String taxfreeamt, String tax,
                               String taxamt) {
        this.goodsname = goodsname;
        this.num = num;
        this.hsbz = hsbz;
        this.price = price;
        this.taxrate = taxrate;
        this.spec = spec;
        this.unit = unit;
        this.spbm = spbm;
        this.zsbm = zsbm;
        this.fphxz = fphxz;
        this.yhzcbs = yhzcbs;
        this.zzstsgl = zzstsgl;
        this.lslbs = lslbs;
        this.taxfreeamt = taxfreeamt;
        this.tax = tax;
        this.taxamt = taxamt;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    public String getGoodsname() {
        return goodsname;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getNum() {
        return num;
    }

    public void setHsbz(String hsbz) {
        this.hsbz = hsbz;
    }

    public String getHsbz() {
        return hsbz;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setTaxrate(String taxrate) {
        this.taxrate = taxrate;
    }

    public String getTaxrate() {
        return taxrate;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getSpec() {
        return spec;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }

    public void setSpbm(String spbm) {
        this.spbm = spbm;
    }

    public String getSpbm() {
        return spbm;
    }

    public void setZsbm(String zsbm) {
        this.zsbm = zsbm;
    }

    public String getZsbm() {
        return zsbm;
    }

    public void setFphxz(String fphxz) {
        this.fphxz = fphxz;
    }

    public String getFphxz() {
        return fphxz;
    }

    public void setYhzcbs(String yhzcbs) {
        this.yhzcbs = yhzcbs;
    }

    public String getYhzcbs() {
        return yhzcbs;
    }

    public void setZzstsgl(String zzstsgl) {
        this.zzstsgl = zzstsgl;
    }

    public String getZzstsgl() {
        return zzstsgl;
    }

    public void setLslbs(String lslbs) {
        this.lslbs = lslbs;
    }

    public String getLslbs() {
        return lslbs;
    }

    public void setTaxfreeamt(String taxfreeamt) {
        this.taxfreeamt = taxfreeamt;
    }

    public String getTaxfreeamt() {
        return taxfreeamt;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getTax() {
        return tax;
    }

    public void setTaxamt(String taxamt) {
        this.taxamt = taxamt;
    }

    public String getTaxamt() {
        return taxamt;
    }
}
