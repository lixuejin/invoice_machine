package com.lxj.invoiceMachine.Bean;

//
public class InvoiceBean {
    private int id;
    private String time;
    private String money;

    public InvoiceBean(int id, String time, String money) {
        this.id = id;
        this.time = time;
        this.money = money;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}
