package com.lxj.invoiceMachine.Bean;

import java.util.List;

public class GetInvoiceUrlBean {
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 订单总价
     */
    private String orderTotal;//
    /**
     * 合计税额
     */
    private String taxTotal;//
    /**
     * 不含税金额
     */
    private String bhtaxTotal;//
    /**
     * 清单标志:0,根据项目名称数，自动产生清单;1,将项目信息打印至清单
     */
    private String qdbz;//
    /**
     * 清单项目名称:打印清单时
     * 对应发票票面项目名称
     */
    private String qdxmmc;
    /**
     * 单据时间
     */
    private String invoicedate;
    /**
     * 备注
     */
    private String message;
    /**
     * 开票人
     */
    private String clerk;
    /**
     * 收款人
     */
    private String payee;
    /**
     * 复核人
     */
    private String checker;
    /**
     * 部门门店 id（诺诺系统中的 id）
     */
    private String deptid;
    /**
     * 开票员 id（诺诺系统中的id）
     */
    private String clerkid;
    /**
     * 成 品 油 标 志 ： 0 非成品油，1 成品油，默认为 0
     */
    private String cpybz;
    /**
     * 明细数据
     */
    private List<CommodityOrdersBean> commodityOrders;

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderTotal(String orderTotal) {
        this.orderTotal = orderTotal;
    }

    public String getOrderTotal() {
        return orderTotal;
    }

    public void setTaxTotal(String taxTotal) {
        this.taxTotal = taxTotal;
    }

    public String getTaxTotal() {
        return taxTotal;
    }

    public void setBhtaxTotal(String bhtaxTotal) {
        this.bhtaxTotal = bhtaxTotal;
    }

    public String getBhtaxTotal() {
        return bhtaxTotal;
    }

    public void setQdbz(String qdbz) {
        this.qdbz = qdbz;
    }

    public String getQdbz() {
        return qdbz;
    }

    public void setQdxmmc(String qdxmmc) {
        this.qdxmmc = qdxmmc;
    }

    public String getQdxmmc() {
        return qdxmmc;
    }

    public void setInvoicedate(String invoicedate) {
        this.invoicedate = invoicedate;
    }

    public String getInvoicedate() {
        return invoicedate;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setClerk(String clerk) {
        this.clerk = clerk;
    }

    public String getClerk() {
        return clerk;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public String getPayee() {
        return payee;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    public String getChecker() {
        return checker;
    }

    public void setDeptid(String deptid) {
        this.deptid = deptid;
    }

    public String getDeptid() {
        return deptid;
    }

    public void setClerkid(String clerkid) {
        this.clerkid = clerkid;
    }

    public String getClerkid() {
        return clerkid;
    }

    public void setCpybz(String cpybz) {
        this.cpybz = cpybz;
    }

    public String getCpybz() {
        return cpybz;
    }

    public void setCommodityOrders(List<CommodityOrdersBean> commodityOrders) {
        this.commodityOrders = commodityOrders;
    }

    public List<CommodityOrdersBean> getCommodityOrders() {
        return commodityOrders;
    }
}
