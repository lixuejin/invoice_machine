package com.lxj.invoiceMachine.Bean;

public class CommodityPriceBean {
    private float commodityPrice;//油品价格
    private String commodityName;//油品名称
    private float taxRate;//税率
    private float commodityHsbz;//含税标志
    private String commodityUnit;//单位
    private String commodityInvoiceName;//开票商品名

    public String getCommodityInvoiceName() {
        return commodityInvoiceName;
    }

    public void setCommodityInvoiceName(String commodityInvoiceName) {
        this.commodityInvoiceName = commodityInvoiceName;
    }

    public String getCommodityUnit() {
        return commodityUnit;
    }

    public void setCommodityUnit(String commodityUnit) {
        this.commodityUnit = commodityUnit;
    }

    public float getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(float taxRate) {
        this.taxRate = taxRate;
    }

    public float getCommodityHsbz() {
        return commodityHsbz;
    }

    public void setCommodityHsbz(float commodityHsbz) {
        this.commodityHsbz = commodityHsbz;
    }


    public float getCommodityPrice() {
        return commodityPrice;
    }

    public void setCommodityPrice(float commodityPrice) {
        this.commodityPrice = commodityPrice;
    }

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

}
