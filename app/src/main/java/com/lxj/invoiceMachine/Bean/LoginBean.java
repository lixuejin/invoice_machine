package com.lxj.invoiceMachine.Bean;

import java.util.List;

public class LoginBean {
    private List<CommodityPriceBean> commodityList;//油品和油价
    private String token;
    private List<OilGunBean> oilGunList;//油枪号和油品
    private String invoicePassWord;//开票密码
    private String realName;//用户名
    private String taxId;//税号
    private String businessName;//商户名,
    private String businessPhone;//商户电话


    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }


    public List<CommodityPriceBean> getCommodityList() {
        return commodityList;
    }

    public void setCommodityList(List<CommodityPriceBean> commodityList) {
        this.commodityList = commodityList;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<OilGunBean> getOilGunList() {
        return oilGunList;
    }

    public void setOilGunList(List<OilGunBean> oilGunList) {
        this.oilGunList = oilGunList;
    }

    public String getInvoicePassWord() {
        return invoicePassWord;
    }

    public void setInvoicePassWord(String invoicePassWord) {
        this.invoicePassWord = invoicePassWord;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }



}
